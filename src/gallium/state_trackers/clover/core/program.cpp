//
// Copyright 2012 Francisco Jerez
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
// OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
// ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//

#include "core/program.hpp"
#include "llvm/invocation.hpp"
#include "tgsi/invocation.hpp"
#include "spirv/invocation.hpp"

#include <cstring>

using namespace clover;

program::program(clover::context &ctx, const std::string &source) :
   has_source(true), has_il(false), il_type(0u), context(ctx), _source(source),
   _kernel_ref_counter(0), _il(nullptr), _length(0) {
}

program::program(clover::context &ctx,
                 const ref_vector<device> &devs,
                 const std::vector<module> &binaries) :
   has_source(false), has_il(false), il_type(0u), context(ctx),
   _devices(devs), _kernel_ref_counter(0), _il(nullptr), _length(0) {
   for_each([&](device &dev, const module &bin) {
         _builds[&dev] = { bin };
      },
      devs, binaries);
}

program::program(clover::context &ctx, const void *il, const size_t length,
                 const uint32_t type) :
   has_source(false), has_il(true), il_type(type), context(ctx),
   _kernel_ref_counter(0), _il(nullptr), _length(length) {
   const char *c_il = reinterpret_cast<const char *>(il);
   _il = spirv::spirv_to_cpu(c_il, length);
}

program::~program() {
   if (has_il)
      delete[] reinterpret_cast<const char *>(_il);
}

void
program::compile(const ref_vector<device> &devs, const std::string &opts,
                 const header_map &headers) {
   if (has_source) {
      _devices = devs;

      for (auto &dev : devs) {
         std::string log;

         try {
            module m;
            switch (dev.ir_format())
            {
               case PIPE_SHADER_IR_TGSI:
                  m = tgsi::compile_program(_source, log);
                  break;
               case PIPE_SHADER_IR_SPIRV:
                  m = spirv::compile_program(_source, headers,
                                             dev.ir_target(), opts, log);
                  break;
               case PIPE_SHADER_IR_LLVM:
               default:
                  m = llvm::compile_program(_source, headers,
                                            dev.ir_target(), opts, log);
                  break;
            }
            _builds[&dev] = { m, opts, log };
         } catch (...) {
            _builds[&dev] = { module(), opts, log };
            throw;
         }
      }
   } else if (has_il) {
      _devices = devs;

      for (auto &dev : devs) {
         std::string log;

         try {
            if (il_type == PIPE_SHADER_IR_SPIRV) {
               if (!dev.supports_ir(PIPE_SHADER_IR_SPIRV)) {
                  log = "Device does not support SPIR-V as IL\n";
                  throw error(CL_INVALID_BINARY);
               }
               _builds[&dev] = { spirv::process_program(_il, _length, false, log), opts, log };
            } else {
               log = "Only SPIR-V is supported as IL by clover for now\n";
               throw error(CL_INVALID_BINARY);
            }
         } catch (const error &) {
            _builds[&dev] = { module(), opts, log };
            throw;
         }
      }
   }
}

void
program::link(const ref_vector<device> &devs, const std::string &opts,
              const ref_vector<program> &progs) {
   _devices = devs;

   for (auto &dev : devs) {
      const std::vector<module> ms = map([&](const program &prog) {
         return prog.build(dev).binary;
         }, progs);
      std::string log = _builds[&dev].log;

      try {
         module m;
         switch (dev.ir_format()) {
         case PIPE_SHADER_IR_TGSI:
            m = tgsi::link_program(ms);
            break;
         case PIPE_SHADER_IR_SPIRV:
            m = clover::spirv::link_program(ms, opts, log);
            break;
         case PIPE_SHADER_IR_LLVM:
         default:
            m = llvm::link_program(ms, dev.ir_format(),
                                   dev.ir_target(), opts, log);
            break;
         }
         _builds[&dev] = { m, opts, log };
      } catch (...) {
         _builds[&dev] = { module(), opts, log };
         throw;
      }
   }
}

const void *
program::il() const {
   return _il;
}

size_t
program::length() const {
   return _length;
}

const std::string &
program::source() const {
   return _source;
}

program::device_range
program::devices() const {
   return map(evals(), _devices);
}

cl_build_status
program::build::status() const {
   if (!binary.secs.empty())
      return CL_BUILD_SUCCESS;
   else if (log.size())
      return CL_BUILD_ERROR;
   else
      return CL_BUILD_NONE;
}

cl_program_binary_type
program::build::binary_type() const {
   if (any_of(type_equals(module::section::text_intermediate), binary.secs))
      return CL_PROGRAM_BINARY_TYPE_COMPILED_OBJECT;
   else if (any_of(type_equals(module::section::text_library), binary.secs))
      return CL_PROGRAM_BINARY_TYPE_LIBRARY;
   else if (any_of(type_equals(module::section::text_executable), binary.secs))
      return CL_PROGRAM_BINARY_TYPE_EXECUTABLE;
   else
      return CL_PROGRAM_BINARY_TYPE_NONE;
}

const struct program::build &
program::build(const device &dev) const {
   static const struct build null;
   return _builds.count(&dev) ? _builds.find(&dev)->second : null;
}

const std::vector<module::symbol> &
program::symbols() const {
   if (_builds.empty())
      throw error(CL_INVALID_PROGRAM_EXECUTABLE);

   return _builds.begin()->second.binary.syms;
}

unsigned
program::kernel_ref_count() const {
   return _kernel_ref_counter.ref_count();
}
