//
// Copyright 2017 Pierre Moreau
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
// OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
// ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//

#include <cstring>

#include <iostream>
#include <stack>
#include <tuple>
#include <unordered_map>
#include <vector>

#include <clang/Frontend/CompilerInstance.h>
#include <clang/Frontend/TextDiagnosticBuffer.h>
#include <clang/Frontend/TextDiagnosticPrinter.h>
#include <clang/CodeGen/CodeGenAction.h>
#include <clang/Basic/TargetInfo.h>
#include <llvm/Bitcode/BitstreamWriter.h>
#include <llvm/Bitcode/ReaderWriter.h>
#include <llvm/Linker/Linker.h>
#include <llvm/IR/DiagnosticInfo.h>
#include <llvm/IR/DiagnosticPrinter.h>
#include <llvm/IR/DerivedTypes.h>
#include <llvm/IR/LLVMContext.h>
#include <llvm/IR/Module.h>
#include <llvm/Support/SourceMgr.h>
#include <llvm/IRReader/IRReader.h>
#include <llvm/Support/CodeGen.h>
#include <llvm/Support/TargetSelect.h>
#include <llvm/Support/MemoryBuffer.h>
#include <llvm/Support/FormattedStream.h>
#include <llvm/Support/TargetRegistry.h>
#include <llvm/Support/SPIRV.h>
#include <llvm/Transforms/IPO.h>
#include <llvm/Transforms/IPO/PassManagerBuilder.h>
#include <llvm/Transforms/Utils/Cloning.h>


#include <llvm/IR/DataLayout.h>
#if HAVE_LLVM >= 0x0307
#include <llvm/Analysis/TargetLibraryInfo.h>
#else
#include <llvm/Target/TargetLibraryInfo.h>
#endif
#include <llvm/Target/TargetMachine.h>
#include <llvm/Target/TargetOptions.h>

#include <llvm-c/Target.h>
#include <llvm-c/TargetMachine.h>
#include <llvm-c/Core.h>
#include <spirv-tools/libspirv.hpp>
#include <spirv-tools/linker.hpp>

#include "core/error.hpp"
#include "invocation.hpp"
#include "llvm/util.hpp"
#include "pipe/p_state.h"
#include "util/algorithm.hpp"
#include "util/functional.hpp"
#include "util/u_debug.h"
#include "util/u_math.h"

#include "spirv.hpp11"

#define SPIRV_MODULE_TYPE_INTERMEDIATE 0u
#define SPIRV_MODULE_TYPE_EXECUTABLE   1u
#define SPIRV_MODULE_TYPE_LIBRARY      2u
#define SPIRV_MODULE_TYPE_MASK       0x3u
#define SPIRV_FLAG_ENABLE_LINK_OPTIONS (1u << 31u)

using namespace clover;

namespace {

   template<typename T>
   T get(const char* source, size_t index) {
      return static_cast<T>(reinterpret_cast<const uint32_t*>(source)[index]);
   }

   template<typename T>
   T get(const std::vector<char>& source, size_t index) {
      return get<T>(source.data(), index);
   }

   void set(char* source, size_t index, uint32_t value) {
      reinterpret_cast<uint32_t*>(source)[index] = value;
   }

   enum module::argument::type
   convertStorageClass(spv::StorageClass storage_class, std::string &err) {
      switch (storage_class) {
      case spv::StorageClass::Function:
         return module::argument::scalar;
      case spv::StorageClass::UniformConstant:
         return module::argument::constant;
      case spv::StorageClass::Workgroup:
         return module::argument::local;
      case spv::StorageClass::CrossWorkgroup:
         return module::argument::global;
      default:
         err += "Invalid storage type " +
                std::to_string(static_cast<int>(storage_class)) + "\n";
         throw build_error();
      }
   }

   enum module::argument::type
   convertImageType(spv::Id id, spv::Dim dim, spv::AccessQualifier access,
                    std::string &err) {
#define APPEND_DIM(d) \
      switch(access) { \
      case spv::AccessQualifier::ReadOnly: \
         return module::argument::image##d##_rd; \
      case spv::AccessQualifier::WriteOnly: \
         return module::argument::image##d##_wr; \
      default: \
         err += "Unsupported access qualifier " #d " for image " + \
                std::to_string(static_cast<int>(id)); \
         throw build_error(); \
      }

      switch (dim) {
      case spv::Dim::Dim2D:
         APPEND_DIM(2d)
      case spv::Dim::Dim3D:
         APPEND_DIM(3d)
      default:
         err += "Unsupported dimension " + std::to_string(static_cast<int>(dim)) +
                " for image " + std::to_string(static_cast<int>(id));
         throw build_error();
      }

#undef APPEND_DIM
   }

#define GET_OPERAND(type, operand_id) get<type>(source, i + operand_id)

   void
   extract_kernels_arguments(module &m, std::string &err,
                             const std::vector<char> &source) {
      const unsigned int length = source.size() / sizeof(uint32_t);
      unsigned int i = 5u; // Skip header

      std::string kernel_name;
      uint32_t kernel_nb = 0u;
      std::vector<module::argument> args;
      uint32_t pointer_size = 0u;

      std::unordered_map<spv::Id, std::string> kernels;
      std::unordered_map<spv::Id, module::argument> types;
      std::unordered_map<spv::Id, spv::Id> pointerTypes;
      std::unordered_map<spv::Id, unsigned int> constants;
      std::unordered_map<spv::Id, std::unordered_map<spv::Decoration, std::vector<std::vector<uint32_t>>>> decorations;

      while (i < length) {
         const auto desc_word = get<uint32_t>(source, i);
         const auto opcode = static_cast<spv::Op>(desc_word & spv::OpCodeMask);
         const unsigned int num_operands = desc_word >> spv::WordCountShift;

         switch (opcode) {
         case spv::Op::OpEntryPoint:
            if (GET_OPERAND(spv::ExecutionModel, 1) != spv::ExecutionModel::Kernel)
               break;
            kernels.emplace(GET_OPERAND(spv::Id, 2), source.data() + (i + 3) * sizeof(uint32_t));
            break;

         case spv::Op::OpMemoryModel:
            switch (GET_OPERAND(spv::AddressingModel, 1)) {
            case spv::AddressingModel::Physical32:
               pointer_size = 32u / 8u;
               break;
            case spv::AddressingModel::Physical64:
               pointer_size = 64u / 8u;
               break;
            case spv::AddressingModel::Logical:
               err += "Addressing model 'Logical' is not valid for OpenCL\n";
               // FALLTHROUGH
            case spv::AddressingModel::Max:
               throw build_error();
            }
            break;

         case spv::Op::OpDecorate: {
            const auto decoration = GET_OPERAND(spv::Decoration, 2);
            std::vector<uint32_t> words;
            for (unsigned int j = 3u; j < num_operands; ++j)
               words.push_back(GET_OPERAND(uint32_t, j));
            decorations[GET_OPERAND(spv::Id, 1)][decoration].emplace_back(words);
            break;
         }

         case spv::Op::OpGroupDecorate: {
            const auto &groupDecorations = decorations[GET_OPERAND(spv::Id, 1)];
            for (unsigned int j = 2u; j < num_operands; ++j) {
               auto &idDecorations = decorations[GET_OPERAND(uint32_t, j)];
               for (const auto &k : groupDecorations)
                  idDecorations[k.first].insert(idDecorations[k.first].end(), k.second.begin(), k.second.end());
            }
            break;
         }

         case spv::Op::OpConstant:
            // We only care about constants that represents the size of arrays.
            // If they are passed as argument, they will never be more than
            // 4GB-wide, and even if they did, a clover::module::argument size
            // is represented by an int.
            constants[GET_OPERAND(spv::Id, 2)] = GET_OPERAND(unsigned int, 3u);
            break;

         case spv::Op::OpTypeInt: // FALLTHROUGH
         case spv::Op::OpTypeFloat: {
            const auto size = GET_OPERAND(uint32_t, 2) / 8u;
            types[GET_OPERAND(spv::Id, 1)] = { module::argument::scalar,
                                               size, size, size,
                                               module::argument::zero_ext };
            break;
         }

         case spv::Op::OpTypeArray: {
            const auto id = GET_OPERAND(spv::Id, 1);
            const auto type_id = GET_OPERAND(spv::Id, 2);
            const auto types_iter = types.find(type_id);
            if (types_iter == types.end()) {
               err += "Type " + std::to_string(type_id) + " is missing\n";
               throw build_error();
            }
            const auto constant_id = GET_OPERAND(spv::Id, 3);
            const auto constants_iter = constants.find(constant_id);
            if (constants_iter == constants.end()) {
               err += "Constant " + std::to_string(constant_id) + " is missing\n";
               throw build_error();
            }
            const auto elem_size = types_iter->second.size;
            const auto elem_nbs = constants_iter->second;
            const auto size = elem_size * elem_nbs;
            types[id] = { module::argument::scalar, size, size,
                          types_iter->second.target_align,
                          module::argument::zero_ext };
            break;
         }

         case spv::Op::OpTypeStruct: {
            const auto id = GET_OPERAND(spv::Id, 1);
            const auto &type_decorations = decorations.find(id);
            const auto is_packed = (type_decorations != decorations.end()) ?
                                   type_decorations->second.find(spv::Decoration::CPacked) != type_decorations->second.end() :
                                   false;

            unsigned struct_size = 0u;
            unsigned max_elem_size = 0u;
            for (unsigned j = 2u; j < num_operands; ++j) {
               const auto type_id = GET_OPERAND(spv::Id, j);
               const auto types_iter = types.find(type_id);
               if (types_iter == types.end()) {
                  err += "Type " + std::to_string(type_id) + " is missing\n";
                  throw build_error();
               }
               const auto alignment = is_packed ? 1u : types_iter->second.target_align;
               const auto padding = (-struct_size) & (alignment - 1u);
               struct_size += padding + types_iter->second.target_size;
               max_elem_size = std::max(max_elem_size, types_iter->second.target_align);
            }
            if (!is_packed)
               struct_size += (-struct_size) & (max_elem_size - 1u);

            types[id] = { module::argument::scalar, struct_size, struct_size, is_packed ? 1u : max_elem_size, module::argument::zero_ext };
            break;
         }

         case spv::Op::OpTypeVector: {
            const auto id = GET_OPERAND(spv::Id, 1);
            const auto type_id = GET_OPERAND(spv::Id, 2);
            const auto types_iter = types.find(type_id);
            if (types_iter == types.end()) {
               err += "Type " + std::to_string(type_id) + " is missing\n";
               throw build_error();
            }
            const auto elem_size = types_iter->second.size;
            const auto elem_nbs = GET_OPERAND(uint32_t, 3);
            const auto size = elem_size * elem_nbs;
            types[id] = { module::argument::scalar,
                                               size, size, elem_size,
                                               module::argument::zero_ext };
            break;
         }

         case spv::Op::OpTypePointer: {
            const auto id = GET_OPERAND(spv::Id, 1);
            const auto storage_class = GET_OPERAND(spv::StorageClass, 2);
            types[id] = { convertStorageClass(storage_class, err), sizeof(cl_mem) };
            pointerTypes[id] = GET_OPERAND(spv::Id, 3);
            break;
         }

         case spv::Op::OpTypeSampler:
            types[GET_OPERAND(spv::Id, 1)] = { module::argument::sampler, sizeof(cl_sampler) };
            break;

         case spv::Op::OpTypeImage: {
            const auto id = GET_OPERAND(spv::Id, 1);
            const auto dim = GET_OPERAND(spv::Dim, 3);
            const auto access = GET_OPERAND(spv::AccessQualifier, 9);
            types[id] = { convertImageType(id, dim, access, err), sizeof(cl_mem) };
            break;
         }

         // case spv::Op::OpTypePipe: OpenCL 2.0
         // case spv::Op::OpTypeQueue: OpenCL 2.0

         case spv::Op::OpFunction: {
            const auto kernels_iter = kernels.find(GET_OPERAND(spv::Id, 2));
            if (kernels_iter != kernels.end())
               kernel_name = kernels_iter->second;
            break;
         }

         case spv::Op::OpFunctionParameter: {
            if (kernel_name.empty())
               break;
            const auto type_id = GET_OPERAND(spv::Id, 1);
            const auto types_iter = types.find(type_id);
            if (types_iter == types.end()) {
               err += "Type " + std::to_string(type_id) + " is missing\n";
               throw build_error();
            }
            const auto &decos = decorations[GET_OPERAND(spv::Id, 2)];
            const auto &funcParamAttrItr = decos.find(spv::Decoration::FuncParamAttr);
            auto arg = types_iter->second;
            if (funcParamAttrItr != decos.end()) {
               for (auto &j : funcParamAttrItr->second) {
                  switch (static_cast<spv::FunctionParameterAttribute>(j[0])) {
                  case spv::FunctionParameterAttribute::Sext:
                     arg.ext_type = module::argument::sign_ext;
                     break;
                  case spv::FunctionParameterAttribute::Zext:
                     arg.ext_type = module::argument::zero_ext;
                     break;
                  case spv::FunctionParameterAttribute::ByVal: {
                     const auto ptrIter = pointerTypes.find(type_id);
                     if (ptrIter == pointerTypes.end()) {
                        err += "Failed to find pointer " + std::to_string(type_id) + "\n";
                        throw build_error();
                     }
                     const auto pointedTypeIter = types.find(ptrIter->second);
                     if (pointedTypeIter == types.end()) {
                        err += "Type " + std::to_string(ptrIter->second) + " is missing\n";
                        throw build_error();
                     }
                     arg = pointedTypeIter->second;
                     break;
                  }
                  }
               }
            }
            std::cout << arg.size << "\n";
            args.emplace_back(arg);
            break;
         }

         case spv::Op::OpFunctionEnd:
            if (kernel_name.empty())
               break;
            m.syms.emplace_back(kernel_name, 0, kernel_nb, args);
            ++kernel_nb;
            kernel_name.clear();
            args.clear();
            break;

         default:
            break;
         }

         i += num_operands;
      }
   }

   enum module::section::type
   guess_binary_type(const std::vector<char> &source, std::string &err) {
      const uint32_t identification_word = get<uint32_t>(source, 4u);
      switch (identification_word & SPIRV_MODULE_TYPE_MASK) {
      case SPIRV_MODULE_TYPE_INTERMEDIATE: return module::section::text_intermediate;
      case SPIRV_MODULE_TYPE_EXECUTABLE:   return module::section::text_executable;
      case SPIRV_MODULE_TYPE_LIBRARY:      return module::section::text_library;
      default:                             return module::section::text_intermediate;
      }
   }

#undef GET_OPERAND

   std::string
   version_to_string(unsigned version) {
      return std::to_string((version >> 16u) & 0xff) + "." +
             std::to_string((version >>  8u) & 0xff);
   }

   void
   check_spirv_version(const char *binary, std::string &r_log) {
      const auto binary_version = get<uint32_t>(binary, 1u);
      if (binary_version <= spv::Version)
         return;

      r_log += "SPIR-V version " + version_to_string(binary_version) +
               " is not supported; supported versions <= " +
               version_to_string(spv::Version);
      _debug_printf("%s\n", r_log.c_str());
      throw build_error();
   }

   std::string
   format_validator_msg(spv_message_level_t level,
                        const spv_position_t& position, const char* message) {
      auto const level_to_string = [](spv_message_level_t level){
#define LVL2STR(lvl) case SPV_MSG_##lvl: return std::string(#lvl)
         switch (level) {
            LVL2STR(FATAL);
            LVL2STR(INTERNAL_ERROR);
            LVL2STR(ERROR);
            LVL2STR(WARNING);
            LVL2STR(INFO);
            LVL2STR(DEBUG);
         }
#undef LVL2STR
         return std::string();
      };
      return "[" + level_to_string(level) + "] At word No." +
             std::to_string(position.index) + ": \"" + message + "\"\n";
   }

   module::section
   make_text_section(const std::vector<char> &code,
                     enum module::section::type section_type) {
      const pipe_llvm_program_header header { uint32_t(code.size()) };
      module::section text { 0, section_type, header.num_bytes, {} };

      text.data.insert(text.data.end(), reinterpret_cast<const char *>(&header),
                       reinterpret_cast<const char *>(&header) + sizeof(header));
      text.data.insert(text.data.end(), code.begin(), code.end());

      return text;
   }

   ::llvm::Module *
   compile_spirv(::llvm::LLVMContext &llvm_ctx, const std::string &source,
                 const header_map &headers,
                 const std::string &name, const std::string &triple,
                 const std::string &processor, const std::string &opts,
                 ::clang::LangAS::Map& address_spaces, unsigned &optimization_level,
                 std::string &r_log) {

      clang::CompilerInstance c;
      clang::EmitSPIRVAction act(&llvm_ctx);
      std::string log;
      ::llvm::raw_string_ostream s_log(log);

      // Parse the compiler options:
      std::vector<std::string> opts_array;
      std::istringstream ss(opts);

      while (!ss.eof()) {
         std::string opt;
         getline(ss, opt, ' ');
         opts_array.push_back(opt);
      }

      opts_array.push_back(name);

      std::vector<const char *> opts_carray;
      opts_carray.push_back("-cc1");
      opts_carray.push_back("-emit-spirv");
      opts_carray.push_back("-cl-std=CL1.2");
      opts_carray.push_back("-includeopencl.h");
      for (unsigned i = 0; i < opts_array.size(); i++) {
         opts_carray.push_back(opts_array.at(i).c_str());
      }

      ::llvm::IntrusiveRefCntPtr<clang::DiagnosticIDs> DiagID;
      ::llvm::IntrusiveRefCntPtr<clang::DiagnosticOptions> DiagOpts;
      clang::TextDiagnosticBuffer *DiagsBuffer;

      DiagID = new clang::DiagnosticIDs();
      DiagOpts = new clang::DiagnosticOptions();
      DiagsBuffer = new clang::TextDiagnosticBuffer();

      clang::DiagnosticsEngine Diags(DiagID, &*DiagOpts, DiagsBuffer);
      bool Success;

      Success = clang::CompilerInvocation::CreateFromArgs(c.getInvocation(),
                                        opts_carray.data(),
                                        opts_carray.data() + opts_carray.size(),
                                        Diags);
      if (!Success) {
         throw error(CL_INVALID_COMPILER_OPTIONS);
      }

      c.getTargetOpts().Triple = triple;

      // This is a workaround otherwise clang doesn't find opencl-12.h
      c.getHeaderSearchOpts().AddPath("@@LLVM_INSTALL_PREFIX@@/lib/clang/3.6.1/include",
                                    clang::frontend::Angled,
                                    false, false
                                    );

      // This is a workaround for a Clang bug which causes the number
      // of warnings and errors to be printed to stderr.
      // http://www.llvm.org/bugs/show_bug.cgi?id=19735
      c.getDiagnosticOpts().ShowCarets = false;
      c.createDiagnostics(
                          new clang::TextDiagnosticPrinter(
                                 s_log,
                                 &c.getDiagnosticOpts()));

#if HAVE_LLVM >= 0x0306
      c.getPreprocessorOpts().addRemappedFile(name,
                                              ::llvm::MemoryBuffer::getMemBuffer(source).release());
#else
      c.getPreprocessorOpts().addRemappedFile(name,
                                      ::llvm::MemoryBuffer::getMemBuffer(source));
#endif

      if (headers.size()) {
         const std::string tmp_header_path = "/tmp/clover/";

         c.getHeaderSearchOpts().AddPath(tmp_header_path,
                                         clang::frontend::Angled,
                                         false, false
                                         );

         for (header_map::const_iterator it = headers.begin();
              it != headers.end(); ++it) {
            const std::string path = tmp_header_path + std::string(it->first);
            c.getPreprocessorOpts().addRemappedFile(path,
#if HAVE_LLVM >= 0x0306
                    ::llvm::MemoryBuffer::getMemBuffer(it->second.c_str()).release());
#else
                    ::llvm::MemoryBuffer::getMemBuffer(it->second.c_str()));
#endif
         }
      }

      optimization_level = c.getCodeGenOpts().OptimizationLevel;

      // Compile the code
      bool ExecSuccess = c.ExecuteAction(act);
      r_log = log;

      if (!ExecSuccess)
         throw build_error();

      // Get address spaces map to be able to find kernel argument address space
      memcpy(address_spaces, c.getTarget().getAddressSpaceMap(),
                                                        sizeof(address_spaces));

#if HAVE_LLVM >= 0x0306
      return act.takeModule().release();
#else
      return act.takeModule();
#endif
   }

   void
   diagnostic_handler(const ::llvm::DiagnosticInfo &di, void *data) {
      if (di.getSeverity() == ::llvm::DS_Error) {
         std::string message = *(std::string*)data;
         ::llvm::raw_string_ostream stream(message);
         ::llvm::DiagnosticPrinterRawOStream dp(stream);
         di.print(dp);
         stream.flush();
         *(std::string*)data = message;

         throw build_error();
      }
   }

   void
   init_targets() {
      static bool targets_initialized = false;
      if (!targets_initialized) {
         LLVMInitializeAllTargets();
         LLVMInitializeAllTargetInfos();
         LLVMInitializeAllTargetMCs();
         LLVMInitializeAllAsmPrinters();
         targets_initialized = true;
      }
   }

   std::vector<char>
   build_module_spirv(::llvm::Module *mod,
                      clang::LangAS::Map& address_spaces,
                      std::string& log) {

      module m;
      std::string err;

      ::llvm::SmallVector<char, 1024> spirv_bitcode;
      ::llvm::raw_svector_ostream bitcode_ostream(spirv_bitcode);
      ::llvm::WriteSPIRV(mod, bitcode_ostream, err);
#if HAVE_LLVM < 0x0308
      bitcode_ostream.flush();
#endif

      std::vector<char> data;
      data.insert(data.end(), spirv_bitcode.begin(),
                                  spirv_bitcode.end());
      return data;
   }
}

bool
clover::spirv::is_binary_spirv(const char *binary)
{
   const uint32_t first_word = get<uint32_t>(binary, 0u);
   return (first_word == spv::MagicNumber) ||
          (util_bswap32(first_word) == spv::MagicNumber);
}

char *
clover::spirv::spirv_to_cpu(const char *binary, size_t length)
{
   uint32_t word = get<uint32_t>(binary, 0u);
   size_t i = 0;
   char *cpu_endianness_binary = new char[length];
   if (word == spv::MagicNumber)
      return reinterpret_cast<char*>(std::memcpy(cpu_endianness_binary, binary, length));

   for (i = 0; i < length; i += 4) { // XXX seems highly wrong!
      word = get<uint32_t>(binary, i);
      set(cpu_endianness_binary, i, util_bswap32(word));
   }

   return cpu_endianness_binary;
}

module
clover::spirv::process_program(const void *il, const size_t length,
                               bool guess_section_type, std::string &r_log) {
   auto c_il = reinterpret_cast<const char*>(il);
   char *cpu_endianness_binary = clover::spirv::spirv_to_cpu(c_il, length);

   auto const validator_consumer = [&r_log](spv_message_level_t level,
                                            const char* /* source */,
                                            const spv_position_t& position,
                                            const char* message) {
      r_log += format_validator_msg(level, position, message);
   };
   spvtools::SpirvTools spvTool(SPV_ENV_OPENCL_2_1);
   spvTool.SetMessageConsumer(validator_consumer);
   if (!spvTool.Validate(reinterpret_cast<const uint32_t*>(cpu_endianness_binary), length / 4u))
      throw build_error();

   check_spirv_version(cpu_endianness_binary, r_log);

   std::vector<char> source(cpu_endianness_binary, cpu_endianness_binary + length);

   // It is our responsability to free the converted result.
   free(cpu_endianness_binary);

   module m;
   extract_kernels_arguments(m, r_log, source);
   auto section_type = module::section::text_intermediate;
   if (guess_section_type)
      section_type = guess_binary_type(source, r_log);
   m.secs.push_back(make_text_section(source, section_type));

   return m;
}

module
clover::spirv::compile_program(const std::string &source,
                               const header_map &headers,
                               const std::string &target,
                               const std::string &opts,
                               std::string &r_log) {
   init_targets();

   size_t processor_str_len = std::string(target).find_first_of("-");
   std::string processor(target, 0, processor_str_len);
   std::string triple(target, processor_str_len + 1,
                      target.size() - processor_str_len - 1);
   clang::LangAS::Map address_spaces;
   ::llvm::LLVMContext llvm_ctx;
   unsigned optimization_level;

   llvm_ctx.setDiagnosticHandler(diagnostic_handler, &r_log);

   // The input file name must have the .cl extension in order for the
   // CompilerInvocation class to recognize it as an OpenCL source file.
   ::llvm::Module *mod = nullptr;
   mod = compile_spirv(llvm_ctx, source, headers, "input.cl",
                        triple, processor, opts, address_spaces,
                        optimization_level, r_log);

   // Build the clover::module
   r_log += std::string("PIPE_SHADER_IR_SPIRV is not really supported yet\n");

   std::vector<char> data = std::move(build_module_spirv(mod, address_spaces, r_log));
#if HAVE_LLVM >= 0x0306
   // LLVM 3.6 and newer, the user takes ownership of the module.
   delete mod;
#endif

   return process_program(data.data(), data.size(), false, r_log);
}

module
clover::spirv::link_program(const std::vector<module> &modules,
                            const std::string &opts, std::string &r_log) {
   std::vector<std::string> options = clover::llvm::tokenize(opts);

   spvtools::LinkerOptions linker_options;
   const bool create_library = count("-create-library", options);
   erase_if(equals("-create-library"), options);

   const bool enable_link = count("-enable-link-options", options);
   erase_if(equals("-enable-link-options"), options);
   if (enable_link && !linker_options.GetCreateLibrary()) {
      r_log += "SPIR-V linker: '-enable-link-options' can't be used without '-create-library'\n";
      throw invalid_build_options_error();
   }

   // Those options are only available when creating an executable, not a
   // library.
   const auto get_opt = [&options,create_library,&r_log](const std::string &name){
      bool var = count(name, options);
      erase_if(equals(name), options);
      if (var && create_library) {
         r_log += "SPIR-V linker: '" + name + "' can't be used with '-create-library'\n";
         throw invalid_build_options_error();
      }
      return var;
   };

   const auto fast_relaxed     = get_opt("-cl-fast-relaxed-math");
   const auto unsafe_math      = get_opt("-cl-unsafe-math-optimizations");
   const auto denorms_are_zero = get_opt("-cl-denorms-are-zero"); // TODO pass this along the binary as not supported by SPIR-V

   if (!options.empty()) {
      r_log += "SPIR-V linker: Invalid linker options: ";
      for (const auto &opt : options)
         r_log += "'" + opt + "' ";
      throw invalid_build_options_error();
   }

   linker_options.SetCreateLibrary(create_library);
//   if (get_opt("-cl-no-signed-zeroes") || unsafe_math || fast_relaxed)
//      linker_options.AddFPFastMathFlag(spvtools::LinkerOptions::FPFastMathMode::NSZ);
//   if (get_opt("-cl-finite-math-only") || fast_relaxed) {
//      linker_options.AddFPFastMathFlag(spvtools::LinkerOptions::FPFastMathMode::Not_Inf);
//      linker_options.AddFPFastMathFlag(spvtools::LinkerOptions::FPFastMathMode::Not_NaN);
//   }
//   if (unsafe_math || fast_relaxed)
//      linker_options.AddFPFastMathFlag(spvtools::LinkerOptions::FPFastMathMode::Enable_MAD);

   module m;

   const auto section_type = create_library ? module::section::text_library :
                                              module::section::text_executable;

   std::vector<const uint32_t *> sections;
   sections.reserve(modules.size());
   std::vector<size_t> lengths;
   lengths.reserve(modules.size());

   auto const validator_consumer = [&r_log](spv_message_level_t level,
                                            const char* /* source */,
                                            const spv_position_t& position,
                                            const char* message) {
      r_log += format_validator_msg(level, position, message);
      std::cerr << format_validator_msg(level, position, message);
   };
   spvtools::SpirvTools spvTool(SPV_ENV_OPENCL_2_1);
   spvTool.SetMessageConsumer(validator_consumer);

   auto found_binary = false;
   for (const auto &mod : modules) {
      const module::section *msec = nullptr;
      try {
         msec = &find(type_equals(module::section::text_intermediate), mod.secs);
      } catch (const std::out_of_range &e) {
         try {
            msec = &find(type_equals(module::section::text_library),
                         mod.secs);
         } catch (const std::out_of_range &e) {
         }
      }
      found_binary |= msec != nullptr;

      // OpenCL 1.2 Specification, Section 5.6.3:
      // > * All programs specified by input_programs contain a compiled binary
      // >   or library for the device. In this case, a link is performed to
      // >   generate a program executable for this device.
      // > * None of the programs contain a compiled binary or library for that
      // >   device. In this case, no link is performed and there will be no
      // >   program executable generated for this device.
      // > * All other cases will return a CL_INVALID_OPERATION error.
      if (found_binary != (msec != nullptr)) {
         r_log += "SPIR-V linker: Some programs have binaries/libraries and some have nothing\n";
         throw error(CL_INVALID_OPERATION);
      }

      const auto c_il = msec->data.data() + sizeof(struct pipe_llvm_program_header);
      const auto length = msec->size;

      if (!spvTool.Validate(reinterpret_cast<const uint32_t*>(c_il), length / sizeof(uint32_t)))
         throw build_error();

      check_spirv_version(c_il, r_log);

      sections.push_back(reinterpret_cast<const uint32_t*>(c_il));
      lengths.push_back(length / sizeof(uint32_t));
   }

   if (!found_binary)
      return m;

   std::vector<uint32_t> linked_binary;

   spvtools::Linker spvLinker(SPV_ENV_OPENCL_2_1);
   spvLinker.SetMessageConsumer(validator_consumer);
   if (spvLinker.Link(sections.data(), lengths.data(), sections.size(), linked_binary, linker_options) != SPV_SUCCESS)
      throw build_error();

   // The fourth word of the SPIR-V header is reserved and is usually 0. Let's
   // use it to store some extra information.
   linked_binary[4u] = (create_library) ? SPIRV_MODULE_TYPE_LIBRARY : SPIRV_MODULE_TYPE_EXECUTABLE;
   if (create_library && enable_link)
      linked_binary[4u] |= SPIRV_FLAG_ENABLE_LINK_OPTIONS;

   if (!spvTool.Validate(linked_binary.data(), linked_binary.size()))
      throw build_error();

   for (const auto &mod : modules)
      m.syms.insert(m.syms.end(), mod.syms.begin(), mod.syms.end());

   m.secs.push_back(make_text_section({ reinterpret_cast<char*>(linked_binary.data()),
            reinterpret_cast<char*>(linked_binary.data()) + linked_binary.size() * sizeof(uint32_t) }, section_type));

   return m;
}
